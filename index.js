const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const jwt = require('jsonwebtoken');

const app = express();

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS, post, get');
header("Access-Control-Max-Age", "3600");
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
header("Access-Control-Allow-Credentials", "true");

app.use(bodyParser.json(), cors());

app.post('/login', (req, res)=>{
  res.header("Access-Control-Allow-Origin", "*");
  const token = jwt.sign({user: 'user name', admin: true}, 'TimSulClientList', {expiresIn: '5d'});
  try{
    if(!(req.body.email === 'test@gmail.com' && req.body.password === '123456')){
      throw new Error('this user not found');
    }
    res.status(200).send({accessToken: token});
  }
  catch(e){
    res.status(400).send('User not found');
    console.log(e);
  }
});

app.get('/clients', function(req, res){
  res.header("Access-Control-Allow-Origin", "*");
  res.send(
    [
      {
        "id": 2,
        "name": "Пётр",
        "surname": "Серёгин",
        "email": "222@222.com",
        "phone": "89123456789",
        "adding_date": 495370121238,
        "status": "current",
        "last_sell": 1595370121238
      },
      {
        "name": "Тимур",
        "surname": "Султанов",
        "email": "555@555.com",
        "phone": "89161111111",
        "id": 5,
        "adding_date": 1055370121238,
        "status": "current",
        "last_sell": 1495370121238
      },
      {
        "name": "Камаз",
        "surname": "Галимов",
        "email": "34787@sdf.com",
        "phone": "8-123-456-78-39",
        "id": 7,
        "adding_date": 35370121238,
        "status": "former",
        "last_sell": 95370121238
      },
      {
        "name": "Вагиз ",
        "surname": "Вагизыч",
        "email": "23555@53455.com",
        "phone": "89161112111",
        "id": 9,
        "adding_date": 1435370121238,
        "status": "current",
        "last_sell": 1635370121238
      },
      {
        "name": "Ильгиз",
        "surname": "Газизыч",
        "email": "134sdf@xn--df-6kc.com",
        "phone": "8-123-426-74-99",
        "id": 10,
        "adding_date": 635370121238,
        "status": "former",
        "last_sell": 635370121238
      },
      {
        "name": "Виктор",
        "surname": "Товаров",
        "email": "787@sdf.com",
        "phone": "1234567899",
        "id": 12,
        "adding_date": 1295370121238,
        "status": "potential",
        "last_sell": 1295370121238
      },
      {
        "name": "Гурбан",
        "surname": "Гурбанов",
        "phone": "8-911-134-23-4",
        "email": "sdf@rte.com",
        "adding_date": 143370121238,
        "id": 13,
        "status": "former",
        "last_sell": 1435370121238
      },
      {
        "name": "Виолетта",
        "surname": "Фиолетова",
        "phone": "8-545-345-34-54",
        "email": "asdfsdf@gtd.com",
        "adding_date": 1135370121238,
        "id": 14,
        "status": "current",
        "last_sell": 1484370121238
      },
      {
        "name": "Ульяна",
        "surname": "Денисова",
        "phone": "8-545-312-31-34",
        "email": "porsdf@asddff.com",
        "adding_date": 1245370121238,
        "id": 15,
        "status": "current",
        "last_sell":1599970121238
      },
      {
        "name": "Романова",
        "surname": "Настя",
        "phone": "8-234-234-23-42",
        "email": "romanova@ya.ru",
        "adding_date": 1535370121238,
        "id": 16,
        "status": "potential",
        "last_sell":1535370121238
      },
      {
        "name": "Тимур",
        "surname": "Иванов",
        "phone": "8-234-234-23-42",
        "email": "asf@asdd.com",
        "adding_date": 1635370121238,
        "id": 17,
        "status": "potential",
        "last_sell": 1635370121238
      }
    ]
  )
})

const port = process.env.PORT || 3000;
app.listen(port, ()=>{
  console.log(`app listening port ${port}`);
});

